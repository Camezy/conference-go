import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://www.pexels.com/v1/search"  # The V1 and extra from Docs
    headers = {"Authorization": PEXELS_API_KEY}  # Passing the -
    ## Value not the string NO ""
    print(city)
    print(state)
    params = {
        "per_page": 1,
        "query": str(city) + " " + str(state),
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    photo = json.loads(response.content)  #This deletes the string
    print(photo["photos"][0]["src"]["original"])
    return {"picture_url": photo["photos"][0]["src"]["original"]}

# def get_weather_data(city, state): # API Get Show_Conference
#     url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}'
#     headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     response = requests.get(url, headers=headers)
#     lon = geo_code[0]["lon"]
#     lat = geo_code[0]["lat"]


#     url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}'
#     headers = { 'Authorization': OPEN_WEATHER_API_KEY}
#     response = requests.get(url, headers=headers)
#     weather_data = json.loads(response.content)
#     d = {
#         "temp": weather_data["main"]["temp"],
#         "description": weather_data["weather"][0]["description"],
#     }
#     return d
